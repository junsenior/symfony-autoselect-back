<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    public function testGetProducts()
    {
        /*метод createClient - создаёт клиент, который имитирует браузер */
        $client = static::createClient();
        $client->request('GET', '/product', ['filter' => 'w']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertArraySubset(['products' => [
            0 => [
                'id' => 5,
                'name' => 'Gerlach, Lueilwitz and Hoeger',
                'add_date' => [
                    'date' => '1975-06-26 18:05:01.000000',
                    'timezone_type' => 3,
                    'timezone' => 'Asia/Krasnoyarsk'
                ],
                'count' => 60
            ]
        ]
        ],
            json_decode($client->getResponse()->getContent(), true));
    }
}
