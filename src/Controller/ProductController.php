<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product", methods="GET")
     */
    public function getProducts(Request $request, ProductRepository $productRepository)
    {
        $filter = $request->query->get('filter');
        $products = $productRepository->findByFilter($filter);

        return $this->json([
            'products' => $products,
        ]);
    }
}
