<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixture extends Fixture
{

    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {

            $product = new Product();
            $product->setName($this->faker->company)
                ->setAddDate($this->faker->dateTime)
                ->setCount(rand(10, 100));
            $manager->persist($product);

            try {
                $manager->flush();
            } catch (\Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
            }
        }
    }
}
